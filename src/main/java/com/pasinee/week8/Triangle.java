package com.pasinee.week8;

public class Triangle {
    public double sideA;
    public double sideB;
    public double sideC;

    public Triangle(double sideA,double sideB,double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double areas() {
        double S = (sideA + sideB + sideC)/2;
        double tanS = (S-sideA)*(S-sideB)*(S-sideC);
        double areas = Math.sqrt(S*tanS);
        return areas;

    }

    public double perimeter() {
        double perimeter = sideA+sideB+sideC;
        return perimeter;
    }

}
