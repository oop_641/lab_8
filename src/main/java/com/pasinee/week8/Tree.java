package com.pasinee.week8;

public class Tree {
    private String name;
    private int x;
    private int y;
    
    public Tree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
    public Tree(String name) {
        this(name, 0, 0);
    }
    public void print() {
        System.out.println(name + " x: " + x + " y: " + y);
    }

}
